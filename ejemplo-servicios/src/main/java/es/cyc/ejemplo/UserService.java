package es.cyc.ejemplo;

import javax.ejb.Stateless;

@Stateless
public class UserService {
	public String saludar(String nombre) {
		return "Hola " + nombre;
	}
}
